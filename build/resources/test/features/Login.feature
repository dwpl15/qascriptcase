Feature: Realizar login com cadastro já criado
  Scenario: Realizar login no site
    Given that I access ConexaoQA
    And I click in the login button
    When I get "user" and "password"
    Then the dashboard screen will be displayed
