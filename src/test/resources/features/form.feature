Feature: Formulário Único Registro
  Scenario: Inserir dados no formulário
    Given that I access the form
    And I click in the new button
    When I get the "cityid" and "cityname"
    Then the register will be include with success


    Scenario: Atualizar dados no formulário
      Given that I access the form
      And the form is in update mode
      When I modify the "cityname" and "stateid" fields
      And click in the update button
      Then the register will be update with success