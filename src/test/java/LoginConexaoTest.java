import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public class LoginConexaoTest {
    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60000, TimeUnit.MILLISECONDS);

    }
    @After
    public void tearDown() {

        driver.quit();
    }

    @Test
    public void loginConexao() {
        driver.get("https://conexaoqa.herokuapp.com/dashboard");
        driver.findElement(By.cssSelector("li:nth-child(5) .hide-sm")).click();
        driver.findElement(By.cssSelector("*[data-test=\"landing-login\"]")).click();
        driver.findElement(By.name("email")).click();
        driver.findElement(By.name("email")).sendKeys("danilo.william15@gmail.com");
        driver.findElement(By.name("password")).click();
        driver.findElement(By.cssSelector("*[data-test=\"login-submit\"]")).click();
    }


}