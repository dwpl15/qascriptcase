import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class form {
    private WebDriver driver;
    @Given("that I access the form")
    public void thatIAccessTheForm() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60000, TimeUnit.MILLISECONDS);
        driver.get("http://92.204.163.42:8092/scriptcase/app/robot/form_city_test/");
    }

    @And("I click in the new button")
    public void iClickInTheNewButton() {
        driver.findElement(By.cssSelector("#sc_b_new_t")).click();
    }

    @When("I get the {string} and {string}")
    public void iGetTheAnd(String cityid, String cityname) {
        driver.findElement(By.cssSelector("#id_sc_field_cityid")).click();
        driver.findElement(By.xpath("//input[@id='id_sc_field_cityid']")).sendKeys("2258");
        driver.findElement(By.cssSelector("#id_sc_field_cityname")).click();
        driver.findElement(By.xpath("//input[@id='id_sc_field_cityname']")).sendKeys("Recife");
        driver.findElement(By.cssSelector("body.scFormPage.sc-app-form:nth-child(2) div.scFormBorder td.scFormToolbar.sc-toolbar-top tbody:nth-child(1) tr:nth-child(1) td.scFormToolbarPadding:nth-child(2) a.sc-unique-btn-2.scButton_ok > span.btn-label")).click();
    }

    @Then("the register will be include with success")
    public void theRegisterWillBeIncludeWithSuccess() {
        //driver.quit();
    }

    //Modo de atualização
    @And("the form is in update mode")
    public void theFormIsInUpdateMode() {
        driver.findElement(By.xpath("//div[contains(text(),'Atualização de city')]")).sendKeys("Atualização de city");
    }

    @When("I modify the {string} and {string} fields")
    public void iModifyTheAndFields(String cityname, String stateid) {
        
    }

    @And("click in the update button")
    public void clickInTheUpdateButton() {
    }

    @Then("the register will be update with success")
    public void theRegisterWillBeUpdateWithSuccess() {
    }
}
