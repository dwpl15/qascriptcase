package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Login {
    private WebDriver driver;
    @Given("that I access ConexaoQA")
    public void thatIAccessConexaoQA() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60000, TimeUnit.MILLISECONDS);
        driver.get("https://conexaoqa.herokuapp.com");
    }

    @And("I click in the login button")
    public void iClickInTheLoginButton() {
        driver.findElement(By.cssSelector("section.landing div.dark-overlay div.landing-inner div.buttons > a.btn.btn-light:nth-child(2)")).click();
    }

    @When("I get {string} and {string}")
    public void iGetAnd(String user, String password) {
        driver.findElement(By.name("email")).click();
        driver.findElement(By.name("email")).sendKeys("danilo.william15@gmail.com");
        driver.findElement(By.name("password")).click();
        driver.findElement(By.name("password")).sendKeys("SC301140");
        driver.findElement(By.cssSelector("*[data-test=\"login-submit\"]")).click();
    }

    @Then("the dashboard screen will be displayed")
    public void theDashboardScreenWillBeDisplayed() {
        //driver.quit();
    }
}
