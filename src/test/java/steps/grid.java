package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class grid {

    private WebDriver driver;
    @Given("that I access a grid")
    public void thatIAccessAGrid() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60000, TimeUnit.MILLISECONDS);
        driver.get("http://92.204.163.42:8092/scriptcase/app/robot/grid_city");
    }

    @When("I click in the next button")
    public void iClickInTheNextButton() {
        driver.findElement(By.cssSelector("body.scGridPage.sc-app-grid:nth-child(2) div.scGridBorder td.scGridTabelaTd table.scGridToolbar tbody:nth-child(1) tr.scGridToolbarPadding_tr td.scGridToolbarPadding:nth-child(2) a.scButton_fontawesome:nth-child(8) > i.icon_fa.fas.fa-arrow-right")).click();
    }

    @Then("the grid will to next page")
    public void theGridWillToNextPage() {
        //driver.quit();
    }
}
